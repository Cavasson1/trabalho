package com.foodBack.FoodBack.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;

import com.foodBack.FoodBack.config.Conexao;
import com.foodBack.FoodBack.model.Usuario;

public class UsuarioDAO {
	private static Connection conexao;
	private static PreparedStatement sqlParametro = null;
	private static ResultSet resultado = null;
	private static String sql;

	public static void cadastro(Usuario objeto) throws SQLException, JSONException {
		try {

			conexao = new Conexao().geraConexao();

			sql = "insert into usuario(nomeUsuario, emailUsuario, senhaUsuario, cidade) values(?, ?, ?, ?)";

			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setString(1, objeto.getNomeUsuario());
			sqlParametro.setString(2, objeto.getEmailUsuario());
			sqlParametro.setString(3, objeto.getSenhaUsuario());
			sqlParametro.setString(4, objeto.getCidade());

			sqlParametro.executeUpdate();

		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	public static Usuario login(Usuario objeto) throws SQLException, JSONException {
		try {

			conexao = new Conexao().geraConexao();

			sql = "select * from usuario where emailUsuario = ? and senhaUsuario = ?";

			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setString(1, objeto.getEmailUsuario());
			sqlParametro.setString(2, objeto.getSenhaUsuario());

			resultado = sqlParametro.executeQuery();

			if (resultado.next()) {
				Usuario login = new Usuario();
				login.setNomeUsuario(resultado.getString("nomeUsuario"));
				login.setIdUsuario(resultado.getLong("idUsuario"));
				login.setTipoUsuario(resultado.getLong("tipoUsuario"));
				return login;
			}
			return null;

		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	public static List<Usuario> list() throws SQLException, JSONException {
		try {

			conexao = new Conexao().geraConexao();

			sql = "select * from usuario";

			sqlParametro = conexao.prepareStatement(sql);

			resultado = sqlParametro.executeQuery();

			List<Usuario> usuarios = new ArrayList<>();
			while (resultado.next()) {
				Usuario usuario = new Usuario();
				usuario.setNomeUsuario(resultado.getString("nomeUsuario"));
				usuario.setEmailUsuario(resultado.getString("emailUsuario"));
				usuario.setIdUsuario(resultado.getLong("idUsuario"));
				usuarios.add(usuario);
			}
			return usuarios;

		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

}
