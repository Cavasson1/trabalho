package com.foodBack.FoodBack.model;

import java.sql.SQLException;
import java.util.List;

import org.json.JSONException;

import com.foodBack.FoodBack.model.dao.UsuarioDAO;

public class Usuario {
	private Long idUsuario;
	private String nomeUsuario;
	private String emailUsuario;
	private String senhaUsuario;
	private String cidade;
	private Long tipoUsuario;

	public Usuario login() throws SQLException, JSONException {
		return UsuarioDAO.login(this);
	}

	public static List<Usuario> listUsuarios() throws SQLException, JSONException {
		return UsuarioDAO.list();
	}

	public void cadastro() throws SQLException, JSONException {
		UsuarioDAO.cadastro(this);
	}

	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNomeUsuario() {
		return nomeUsuario;
	}

	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}

	public String getEmailUsuario() {
		return emailUsuario;
	}

	public void setEmailUsuario(String emailUsuario) {
		this.emailUsuario = emailUsuario;
	}

	public String getSenhaUsuario() {
		return senhaUsuario;
	}

	public void setSenhaUsuario(String senhaUsuario) {
		this.senhaUsuario = senhaUsuario;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public Long getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(Long tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}
}
