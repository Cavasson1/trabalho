package com.foodBack.FoodBack.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;

import com.foodBack.FoodBack.config.Conexao;
import com.foodBack.FoodBack.model.Estabelecimento;

public class EstabelecimentoDAO {
	private static Connection conexao;
	private static PreparedStatement sqlParametro = null;
	private static ResultSet resultado = null;
	private static String sql;

	public static void cadastro(Estabelecimento objeto) throws SQLException, JSONException {
		conexao = new Conexao().geraConexao();

		try {

			sql = "insert into estabelecimento(nomeEstabelecimento, emailEstabelecimento, senhaEstabelecimento, cidadeEstabelecimento, telefone, horarioAbertura, horarioFechamento) values(?, ?, ?, ?, ?, ?, ?)";

			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setString(1, objeto.getNomeEstabelecimento());
			sqlParametro.setString(2, objeto.getEmailEstabelecimento());
			sqlParametro.setString(3, objeto.getSenhaEstabelecimento());
			sqlParametro.setString(4, objeto.getCidadeEstabelecimento());
			sqlParametro.setString(5, objeto.getTelefone());
			sqlParametro.setString(6, objeto.getHorarioAbertura());
			sqlParametro.setString(7, objeto.getHorarioFechamento());

			sqlParametro.executeUpdate();

		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	public static List<Estabelecimento> list() throws SQLException, JSONException {
		conexao = new Conexao().geraConexao();

		try {

			sql = "select * from estabelecimento";

			sqlParametro = conexao.prepareStatement(sql);

			resultado = sqlParametro.executeQuery();

			List<Estabelecimento> estabelecimentos = new ArrayList<>();
			while (resultado.next()) {
				Estabelecimento estabelecimento = new Estabelecimento();
				estabelecimento.setIdEstabelecimento(resultado.getLong("idEstabelecimento"));
				estabelecimento.setNomeEstabelecimento(resultado.getString("nomeEstabelecimento"));
				estabelecimento.setEmailEstabelecimento(resultado.getString("emailEstabelecimento"));
				estabelecimento.setCidadeEstabelecimento(resultado.getString("cidadeEstabelecimento"));
				estabelecimento.setTelefone(resultado.getString("telefone"));
				estabelecimento.setHorarioAbertura(resultado.getString("horarioAbertura"));
				estabelecimento.setHorarioFechamento(resultado.getString("horarioFechamento"));

				estabelecimentos.add(estabelecimento);
			}
			return estabelecimentos;

		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	public static Estabelecimento detalhes(Long id) throws SQLException, JSONException {
		conexao = new Conexao().geraConexao();

		try {

			sql = "select * from estabelecimento where idEstabelecimento = ?";

			sqlParametro = conexao.prepareStatement(sql);

			sqlParametro.setLong(1, id);

			resultado = sqlParametro.executeQuery();

			if (resultado.next()) {
				Estabelecimento estabelecimento = new Estabelecimento();
				estabelecimento.setIdEstabelecimento(resultado.getLong("idEstabelecimento"));
				estabelecimento.setNomeEstabelecimento(resultado.getString("nomeEstabelecimento"));
				estabelecimento.setEmailEstabelecimento(resultado.getString("emailEstabelecimento"));
				estabelecimento.setCidadeEstabelecimento(resultado.getString("cidadeEstabelecimento"));
				estabelecimento.setTelefone(resultado.getString("telefone"));
				estabelecimento.setHorarioAbertura(resultado.getString("horarioAbertura"));
				estabelecimento.setHorarioFechamento(resultado.getString("horarioFechamento"));

				return estabelecimento;
			}
			return null;

		} finally {
			try {
				sqlParametro.close();
				conexao.close();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

}
