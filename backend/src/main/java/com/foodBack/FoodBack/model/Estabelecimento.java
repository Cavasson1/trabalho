package com.foodBack.FoodBack.model;

import java.sql.SQLException;
import java.util.List;

import org.json.JSONException;

import com.foodBack.FoodBack.model.dao.EstabelecimentoDAO;

public class Estabelecimento {
	private Long idEstabelecimento;
	private String nomeEstabelecimento;
	private String emailEstabelecimento;
	private String senhaEstabelecimento;
	private String cidadeEstabelecimento;
	private String telefone;
	private String horarioAbertura;
	private String horarioFechamento;
	private Double mediaAvaliacoes;

	public static Estabelecimento detalhes(Long id) throws SQLException, JSONException {
		return EstabelecimentoDAO.detalhes(id);
	}

	public static List<Estabelecimento> listEstabelecimento() throws SQLException, JSONException {
		return EstabelecimentoDAO.list();
	}

	public void cadastro() throws SQLException, JSONException {
		EstabelecimentoDAO.cadastro(this);
	}

	public Long getIdEstabelecimento() {
		return idEstabelecimento;
	}

	public void setIdEstabelecimento(Long idEstabelecimento) {
		this.idEstabelecimento = idEstabelecimento;
	}

	public String getNomeEstabelecimento() {
		return nomeEstabelecimento;
	}

	public void setNomeEstabelecimento(String nomeEstabelecimento) {
		this.nomeEstabelecimento = nomeEstabelecimento;
	}

	public String getEmailEstabelecimento() {
		return emailEstabelecimento;
	}

	public void setEmailEstabelecimento(String emailEstabelecimento) {
		this.emailEstabelecimento = emailEstabelecimento;
	}

	public String getSenhaEstabelecimento() {
		return senhaEstabelecimento;
	}

	public void setSenhaEstabelecimento(String senhaEstabelecimento) {
		this.senhaEstabelecimento = senhaEstabelecimento;
	}

	public String getCidadeEstabelecimento() {
		return cidadeEstabelecimento;
	}

	public void setCidadeEstabelecimento(String cidadeEstabelecimento) {
		this.cidadeEstabelecimento = cidadeEstabelecimento;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getHorarioAbertura() {
		return horarioAbertura;
	}

	public void setHorarioAbertura(String horarioAbertura) {
		this.horarioAbertura = horarioAbertura;
	}

	public String getHorarioFechamento() {
		return horarioFechamento;
	}

	public void setHorarioFechamento(String horarioFechamento) {
		this.horarioFechamento = horarioFechamento;
	}

	public Double getMediaAvaliacoes() {
		return mediaAvaliacoes;
	}

	public void setMediaAvaliacoes(Double mediaAvaliacoes) {
		this.mediaAvaliacoes = mediaAvaliacoes;
	}

}
