package com.foodBack.FoodBack.service;

import java.sql.SQLException;
import java.util.List;

import org.json.JSONException;

import com.foodBack.FoodBack.model.Usuario;

public class UsuarioService {

	public void cadastro(Usuario usuario) throws SQLException, JSONException {
		usuario.cadastro();
	}

	public List<Usuario> listUsuarios() throws SQLException, JSONException {
		return Usuario.listUsuarios();
	}
}
