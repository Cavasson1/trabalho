package com.foodBack.FoodBack.service;

import java.sql.SQLException;

import org.json.JSONException;

import com.foodBack.FoodBack.model.Usuario;

public class HomeService {

	public Usuario login(Usuario usuario) throws SQLException, JSONException {
		return usuario.login();
	}

	public Usuario home(Long id) throws SQLException, JSONException {
		return null;
	}
}
