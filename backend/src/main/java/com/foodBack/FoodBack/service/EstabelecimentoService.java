package com.foodBack.FoodBack.service;

import java.sql.SQLException;
import java.util.List;

import org.json.JSONException;

import com.foodBack.FoodBack.model.Estabelecimento;

public class EstabelecimentoService {

	public void cadastro(Estabelecimento estabelecimento) throws SQLException, JSONException {
		estabelecimento.cadastro();
	}

	public Estabelecimento detalhes(Long id) throws SQLException, JSONException {
		return Estabelecimento.detalhes(id);
	}

	public List<Estabelecimento> listEstabelecimento() throws SQLException, JSONException {
		return Estabelecimento.listEstabelecimento();
	}
}
