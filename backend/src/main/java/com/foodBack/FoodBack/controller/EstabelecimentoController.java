package com.foodBack.FoodBack.controller;

import java.sql.SQLException;
import java.util.List;

import org.json.JSONException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.foodBack.FoodBack.model.Estabelecimento;
import com.foodBack.FoodBack.service.EstabelecimentoService;

@RestController
@CrossOrigin
@RequestMapping(value = "/estabelecimento")
public class EstabelecimentoController {

	private EstabelecimentoService service = new EstabelecimentoService();

	@PostMapping(value = { "/cadastro" }, produces = "application/json")
	public void cadastro(@RequestBody Estabelecimento estabelecimento) throws SQLException, JSONException {
		service.cadastro(estabelecimento);
	}

	@GetMapping(value = { "/listar" }, produces = "application/json")
	public List<Estabelecimento> listEstabelecimento() throws SQLException, JSONException {
		return service.listEstabelecimento();
	}

	@GetMapping(value = { "/detalhe/{id}" }, produces = "application/json")
	public Estabelecimento detalhe(@PathVariable Long id) throws SQLException, JSONException {
		return service.detalhes(id);
	}

}
