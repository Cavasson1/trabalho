package com.foodBack.FoodBack.controller;

import java.sql.SQLException;
import java.util.List;

import org.json.JSONException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.foodBack.FoodBack.model.Usuario;
import com.foodBack.FoodBack.service.UsuarioService;

@RestController
@CrossOrigin
@RequestMapping(value = "/usuario")
public class UsuarioController {

	private UsuarioService service = new UsuarioService();

	@PostMapping(value = { "/cadastro" }, produces = "application/json")
	public void cadastro(@RequestBody Usuario usuario) throws SQLException, JSONException {
		service.cadastro(usuario);
	}

	@GetMapping(value = { "/listar" }, produces = "application/json")
	public List<Usuario> listUsuario() throws SQLException, JSONException {
		return service.listUsuarios();
	}

	@GetMapping(value = "/")
	public String index() {
		return "index";
	}
}
