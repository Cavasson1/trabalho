package com.foodBack.FoodBack.controller;

import java.sql.SQLException;

import org.json.JSONException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.foodBack.FoodBack.model.Usuario;
import com.foodBack.FoodBack.service.HomeService;

@RestController
@CrossOrigin
public class HomeController {

	private HomeService service = new HomeService();

	// TODO Refatorar metodo para fazer login pelo objeto Login, que
	// identificará o usuário que está logando e direcionara corretamente
	@PostMapping(value = { "/login" }, produces = "application/json")
	public Usuario login(@RequestBody Usuario cliente) throws SQLException, JSONException {
		return service.login(cliente);
	}

	@PostMapping(value = { "/home/{id}" }, produces = "application/json")
	public Usuario home(@PathVariable Long id) throws SQLException, JSONException {
		return service.home(id);
	}
}
