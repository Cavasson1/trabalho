/* eslint-disable */

import Vue from 'vue';
import Router from 'vue-router';
import login from '@/components/login';
import home from '@/components/home';
import cadastroUsuario from '@/components/usuario/cadastro';
import listagemUsuarios from '@/components/usuario/listagem';
import cadastroEstabelecimento from '@/components/estabelecimento/cadastro';
import listagemEstabelecimento from '@/components/estabelecimento/listagem';
import detalhesEstabelecimento from '@/components/estabelecimento/detalhes';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '*',
      redirect: '/home'
    },
    {
      path: '/',
      redirect: '/home'
    },
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/home',
      name: 'home',
      component: home
    },
    {
      path: '/usuario/cadastro',
      name: 'cadastroUsuario',
      component: cadastroUsuario
    },
    {
      path: '/usuario/listagem',
      name: 'listagemUsuarios',
      component: listagemUsuarios
    },
    {
      path: '/estabelecimento/cadastro',
      name: 'cadastroEstabelecimento',
      component: cadastroEstabelecimento
    },
    {
      path: '/estabelecimento/listagem',
      name: 'listagemEstabelecimento',
      component: listagemEstabelecimento
    },
    {
      path: '/estabelecimento/detalhes',
      name: 'detalhesEstabelecimento',
      component: detalhesEstabelecimento
    }
  ]
});
