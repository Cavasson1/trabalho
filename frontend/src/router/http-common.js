import axios from 'axios'

export const AXIOS = axios.create({
  baseURL: `http://localhost:9001`,
  headers: {
    'Access-Control-Allow-Origin': 'http://localhost:9005'
  }
})
